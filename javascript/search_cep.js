var botao = document.querySelector("#botao-cep")

botao.addEventListener("click",function(event){
    event.preventDefault();

    var cep = document.querySelector("#pesquisa-cep").value

   requestCep(cep.replace(/[^\d]+/g,''))
})

function addCepList(responseCep){
   

    var cepTR = createTr(responseCep)

    var table = document.querySelector("#tabela-cep")

    table.appendChild(cepTR)
    
}


function createTr(respostaCep){

    console.log(respostaCep,"auiiii")
    var trCEP = document.createElement('tr');
    trCEP.classList.add("paciente");

    trCEP.appendChild( createTd(respostaCep.cep,'info-cep'))
    trCEP.appendChild( createTd(respostaCep.logradouro,'info-rua'))
    trCEP.appendChild( createTd(respostaCep.bairro,'info-bairro'))
    trCEP.appendChild( createTd(respostaCep.localidade,'info-cidade'))
    trCEP.appendChild( createTd(respostaCep.uf,'info-estado'))

    return trCEP

}


function createTd(data,classHTML){
    tdCep = document.createElement("td");
    tdCep.textContent = data
    tdCep.classList.add(classHTML)

    return tdCep

}


function iniciaModal(modalID){
    const modal = document.getElementById(modalID)
    modal.classList.add("mostrar")
    modal.addEventListener('click', (e)=>{
         if(e.target.id == modalID || e.target.className == "fechar"){
             modal.classList.remove("mostrar");
             document.location.reload(true);
         }
    })
}

